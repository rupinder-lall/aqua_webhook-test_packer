#!/bin/bash

cloud-init status --wait
sudo DEBIAN_FRONTEND=noninteractive apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y python3-pip
sudo pip3 install ansible boto3 botocore